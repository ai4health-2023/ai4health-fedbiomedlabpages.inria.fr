sphinxcontrib-bibtex==2.5.0
myst-parser>=0.17.0,<1.0.0
jupyter-book==0.15.1

# Frequently Asked Questions

## Will JupyterHub be accessible after the workshop?

Unfortunately, no. We will shutdown the jupyterhub server at the end of each workshop day. 

## What happens if I close a notebook or a terminal tab?

No worries, the underlying process will continue to run. To reopen it, navigate to the jupytehub homepage (the IP address provided [here](/fedbiomed-tutorial/aws-instructions)) and click on the "Running" tab in the top left.

## How to solve "fedbiomed Module Not Found" error

This may be due to the wrong jupyter kernel being selected. In your notebook, under the `Kernel` tab, go to `Change kernel` and select `fedbiomed-researcher`.

## After abruptly closing the node, it automatically starts executing a training when I restart it

This is a common issue, due to the fact that the task was not correctly deleted from the node's queue. Currently, the best option is to delete the folder which contains all of the node's queue information. First, find your node id (you can find it in the `fedbiomed/etc/site_*.ini` files), then remove the folder `rm -r fedbiomed/var/queue_manager_<node id>`

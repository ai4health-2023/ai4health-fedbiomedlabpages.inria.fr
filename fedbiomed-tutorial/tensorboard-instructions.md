# Using Tensorboard during the tutorial

Prerequisites:
- the FL experiment should be ready to run in your jupyter notebook 
- your JupyterHub IP address
- a network port from the table below

## Network ports

### Friday, July 7th

| user | address |
| ---  | ---     |
| francesco | 6006 | 
| lucia | 6007 | 
| yannick\_shaofeng | 6006 | 
| emma | 6007 | 
| celine | 6008 | 
| isabella | 6009 | 
| brice | 6010 | 
| melanie | 6011 | 
| theo | 6012 | 
| javier | 6008 | 
| hugo | 6009 | 
| aleksandar | 6010 | 
| narges | 6011 | 
| charlotte | 6012 | 
| hanae | 6013 | 
| yevgenyi | 6006 | 
| lea | 6007 | 
| theodore | 6008 | 
| takoua | 6009 | 
| paula | 6010 | 
| quoc\_viet | 6011 | 
| franka | 6012 | 

### Thursday, July 6th

| user | port |
| ---  | ---     |
| francesco | 6006  | 
| lucia | 6007  | 
| fouzi | 6008  | 
| david | 6009  | 
| iege | 6010  | 
| idan | 6011  | 
| colleen | 6012  | 
| shambhavi | 6013  | 
| olivier | 6014  | 
| rebeca | 6008  | 
| yannick\_shaofeng | 6009  | 
| charles\_andrew | 6010  | 
| jack | 6011  | 
| abhishek | 6012  | 
| maelys | 6013  | 
| valentin | 6014  | 
| nilesh | 6008  | 
| floriane | 6009  | 
| aymeric | 6010  | 
| stanislas | 6011  | 
| jorge |  6012 | 
| camille | 6013  | 

## Running Tensorboard

Follow the instructions provided in the notebook tutorials. It amounts to finding out in which directory the tensorboard data are saved, and executing
```bash
tensorboard --logdir <PATH TO TENSORBOARD DATA> --host 0.0.0.0 --port <PORT FROM TABLE ABOVE>
```

## Accessing Tensorboard
Copy the IP address that you used to access JupyterHub, and append `:<your port number>` at the end and paste it in your browser. 
For example, if your address was `1.2.3.4` and your port is `8456`, you would insert `http://1.2.3.4:8456` in your browser search bar.


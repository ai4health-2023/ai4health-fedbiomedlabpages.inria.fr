# Instructions for the tutorial

Fed-BioMed is an open-source research and development initiative for translating federated learning into real-world medical applications.
The community of Fed-BioMed gathers experts in medical engineering, machine learning, communication, and security.
We all contribute to provide an open, user-friendly, and trusted framework for deploying the state-of-the-art of federated learning in sensitive environments, such as in hospitals and health data lakes. 

Check out our [fedbiomed.org](https://fedbiomed.org) for the latest documentation and news!

## Connecting to JupyterHub

We provide a ready-to-use Jupyterhub instance running on AWS for you. 
To use it, check the table below and copy the url associated with your username in a browser.

### Friday, July 7th

| user | address |
| ---  | ---     |
| francesco | http://34.251.43.96 | 
| lucia | http://34.251.43.96 | 
| yannick\_shaofeng | http://54.194.207.88 | 
| emma | http://54.194.207.88 | 
| celine | http://54.194.207.88 | 
| isabella | http://54.194.207.88 | 
| brice | http://54.194.207.88 | 
| melanie | http://54.194.207.88 | 
| theo | http://54.194.207.88 | 
| javier | http://34.251.43.96 | 
| hugo | http://34.251.43.96 | 
| aleksandar | http://34.251.43.96 | 
| narges | http://34.251.43.96 | 
| charlotte | http://34.251.43.96 | 
| hanae | http://34.251.43.96 | 
| yevgenyi | http://34.244.73.13 | 
| lea | http://34.244.73.13 | 
| theodore | http://34.244.73.13 | 
| takoua | http://34.244.73.13 | 
| paula | http://34.244.73.13 | 
| quoc\_viet | http://34.244.73.13 | 
| franka | http://34.244.73.13 | 

You may log in with the password provided to you by the workshop presenters.

If you wish, you may also [install](https://fedbiomed.org/latest/tutorials/installation/0-basic-software-installation/) Fed-BioMed locally on your machine. If you have any questions about local installation we can try to quickly help during the workshop, or you may ask support questions through our [mailing list](mailto:fedbiomed-support@inria.fr) and our [user discord channel](https://discord.gg/SWUb7QAS).

## Launching the Fed-BioMed components

In today's tutorial, you will be launching one researcher and two node components.

### The Fed-BioMed node

We have prepare two nodes for you, called `site_1.ini` and `site_2.ini`.
To run them, open two terminals in Jupyterhub and navigate to the fedbiomed folder in both of them
```bash
cd $HOME/fedbiomed
```

Then, in the first terminal execute
```bash
./scripts/fedbiomed_run node config site_1.ini start
```
while in the other terminal you should execute
```bash
./scripts/fedbiomed_run node config site_2.ini start
```

Note that after this command, the terminal is fully dedicated to running the node, and you may not execute other commands on that terminal unless you stop the node's execution beforehand. 

To stop the node, use the `Ctrl+C` combination.


# Fed-BioMed, an open source framework for federated learning in real world healthcare applications
## 2023 AI4Health practical session 
 
This practical session focuses on federated learning (FL) for healthcare applications, and is based on Fed-BioMed, an open source framework for deploying FL in real world use-cases. Throughout the session the participants will get introduced to the basics of federated learning, and will learn to deploy a federated training in a network of clients by using the Fed-BioMed software components. We will focus on the federation of general machine learning approaches for the analysis of medical data (such as tabular or medical images), using a variety of AI frameworks, from Pytorch to scikit-learn. Most advanced topics include the use of privacy-preserving techniques in FL, and the definition of custom data types, models and optimisation routines.	

## Program

The workshop lasts 6 hours, broken into 4 slots of 1.5 hours each. 
The program of the workshop is:

- Introduction to FL and its importance in medical research ([slides](/fedbiomed-tutorial/slides))
- Fed-BioMed introduction and MedNIST tutorial ([notebook](/fedbiomed-tutorial/intro-tutorial-mednist))
- Hands-on exercise: detecting heart disease from tabular data ([notebook](fedbiomed-tutorial/tutorial-sklearn-problem))
- Hands-on exercise: segmentation of brain MRI images ([notebook](fedbiomed-tutorial/brain-segmentation-exercise))

## Using Fed-BioMed during the workshop

We provide a ready-to-use JupyterHub server. Follow the [instructions](/fedbiomed-tutorial/aws-instructions) to find out how to connect. 

## Community

Keep up to date and ask support questions through our [mailing list](mailto:fedbiomed-support@inria.fr) and our [user discord channel](https://discord.gg/SWUb7QAS).

**We welcome new contributors!** Check out our [repo](https://github.com/fedbiomed/fedbiomed) if you are interested.

**We are looking for new collaborations!** Share your research ideas through our [mailing list](mailto:fedbiomed-support@inria.fr) or get in touch with [Francesco](mailto:francesco.cremonesi@inria.fr) directly.
